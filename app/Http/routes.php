<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

/*  Route::get('/', function () {
   //return "<h1>hi<h1>";
    return view('welcome');
});  */

  /*  Route::get('/',function() {
    return view('pages/about');
    //return "hi";
});    */

/* Route::get('/',function() {
    return view('errors/503');
    //return "hi";
}); */

/* Route::get('/users/{id}', function ($id) {
    return "This is user ".$id;
     //return view('welcome');
 }); */
  Route::get('', function () {
   // return "This is user ".$id;
     return view('welcome');
     //return view('index');
 });
Route::auth();

Route::get('/home', 'HomeController@index'); 
Route::get('json',function(){
    return response()->json(['name' => 'Virat Gandhi', 'state' => 'Gujarat']);
    });
    Route::get('view-records','StudViewController@index');
    Route::get('view-users','StudViewController@get_user');//For user
    Route::get('insert','StudInsertController@insertform'); 
    Route::post('create','StudInsertController@insert');

    Route::get('edit-records','StudUpdateController@index'); 
Route::get('edit/{id}','StudUpdateController@show'); 
Route::post('edit/{id}','StudUpdateController@edit'); 
Route::get('ajax',function(){
    return view('message');
 });
 Route::POST('/getmsg','AjaxController@index');
 Route::POST('/getupd','for_update@index'); //For Update on table
 Route::POST('/getinsert','for_update@index_insert'); //For Update on table