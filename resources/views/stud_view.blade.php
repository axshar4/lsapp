<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>SB Admin 2 - Bootstrap Admin Theme</title>

    <!-- Bootstrap Core CSS -->
    <link href="../theme/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../theme/vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- DataTables CSS -->
    <link href="../theme/vendor/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href="../theme/vendor/datatables-responsive/dataTables.responsive.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../theme/dist/css/sb-admin-2.css" rel="stylesheet">
    <link href="../theme/dist/css/custom.css" rel="stylesheet">
    <!-- Custom Fonts -->
    <link href="../theme/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <meta name="csrf-token" content="<?php echo csrf_token() ?>" />    
  <script src = "https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js">
  </script>

  <script type="text/javascript">
        $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
        });
  </script>

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0;background-color: #9A1031;">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                
                <img src="/lsapp/resources/views/UB_icon.png" style="height: 54px;padding-right: 0px;border-left-width: 0px;margin-left: 61px;">
            </div>
            <!-- /.navbar-header -->

            <ul class="nav navbar-top-links navbar-right">
                
                        
                        
                <!-- /.dropdown -->
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-bell fa-fw" style="color:white;"></i> <i class="fa fa-caret-down" style="color:white;"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-alerts">
                        <li>
                            <a href="#">
                                <div>
                                    <i class="fa fa-comment fa-fw"></i> New Comment
                                    <span class="pull-right text-muted small">4 minutes ago</span>
                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#">
                                <div>
                                    <i class="fa fa-twitter fa-fw"></i> 3 New Followers
                                    <span class="pull-right text-muted small">12 minutes ago</span>
                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#">
                                <div>
                                    <i class="fa fa-envelope fa-fw"></i> Message Sent
                                    <span class="pull-right text-muted small">4 minutes ago</span>
                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#">
                                <div>
                                    <i class="fa fa-tasks fa-fw"></i> New Task
                                    <span class="pull-right text-muted small">4 minutes ago</span>
                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#">
                                <div>
                                    <i class="fa fa-upload fa-fw"></i> Server Rebooted
                                    <span class="pull-right text-muted small">4 minutes ago</span>
                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a class="text-center" href="#">
                                <strong>See All Alerts</strong>
                                <i class="fa fa-angle-right"></i>
                            </a>
                        </li>
                    </ul>
                    <!-- /.dropdown-alerts -->
                </li>
                <!-- /.dropdown -->
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw" style="color:white;"></i> <i class="fa fa-caret-down" style="color:white;"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="#" style="background-color: #9A1031;color:white"><i class="fa fa-user fa-fw"></i> User Profile</a>
                        </li>
                        <li class="divider"></li>
                        <li><a href="../../public/login"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->
            </ul>
            <!-- /.navbar-top-links -->

            <div class="navbar-default sidebar" role="navigation" style="background-color: #9A1031;">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        
                        <li>
                            <a href="index.html" style="background-color: #9A1031;color:white"><i class="glyphicon glyphicon-th-large"></i> Dashboard</a>
                        </li>
                        <li>
                            <a href="index.html" style="background-color: #9A1031;color:white"><i class="fa fa-dashboard fa-fw"></i> Order</a>
                        </li>
                        <li>
                            <a href="index.html" style="background-color: #9A1031;color:white"><i class="glyphicon glyphicon-cloud-upload"></i> Upload</a>
                        </li>
                        
                       
                        <li>
                            <a href="#" style="background-color: #9A1031;color:white"><i class="glyphicon glyphicon-comment"></i> Procurement<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="panels-wells.html" style="background-color: #9A1031;color:white">Agmark Price</a>
                                </li>
                                <li>
                                    <a href="buttons.html" style="background-color: #9A1031;color:white">Daily Price</a>
                                </li>
                                <li>
                                    <a href="#" style="background-color: #9A1031;color:white">Auction</a>
                                </li>
                                <li>
                                    <a href="notifications.html" style="background-color: #9A1031;color:white">Offline</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <li>
                            <a href="#" style="background-color: #9A1031;color:white"><i class="glyphicon glyphicon-home"></i> Master Data<span class="fa arrow"></span></a>
                            <ul class="nav nav-third-level">
                            <li>
                                <a href="{{ url('view-records') }}" style="background-color: #9A1031;color:white">Mandi Details</a>
                            </li>
                            <li>
                                <a href="{{ url('view-users') }}" style="background-color: #9A1031;color:white">User Details</a>
                            </li>
                            <li>
                                <a href="{{ url('view-users') }}" style="background-color: #9A1031;color:white">Mandi User Mapping</a>
                            </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        
                        <li>
                            <a href="notifications.html" style="background-color: #9A1031;color:white"><i class="glyphicon glyphicon-calendar"></i> Holiday</a>
                        </li>
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>
            
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12"><br><br>
                <div class="panel panel-default">
                    <div class="panel-body" style="color:#9A1031"><Strong>Mandi Details</Strong></div>
                </div>
                    
                    <button style="float: right;" class="btn btn-danger" id='modal' data-toggle="modal" data-target="#myModal">Add Record</button>    
               <br><br>  </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                       
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                           
                            <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead>
                                    <tr bgcolor="#9A1031">
                                        <th class="custom_color">Mandi Name</th>
                                        <th class="custom_color">Agmark ID</th>
                                        <th class="custom_color">Address</th>
                                        <th class="custom_color">Address 2</th>
                                        <th class="custom_color">Pincode</th>
                                       <!-- <th>Action</th>-->
                                    </tr>
                                </thead>
                                <tbody>
                                <?php
                                $count=1;?>
                                @foreach ($users as $user)
                                <?php
                                $id= $user->id ; 
                                
                               
                                ?>

                                    <tr class="odd gradeX">                                     
                                        <td contentEditable='true' onClick="showEdit(this);" onBlur="saveToDatabase(this,'mandi_name','<?php echo $id; ?>')">{{ $user->mandi_name }}</td>
                                        <td contentEditable='true' onClick="showEdit(this);" onBlur="saveToDatabase(this,'agmark_market_id','<?php echo $id; ?>')">{{ $user->agmark_market_id }}</td>
                                        <td contentEditable='true' onClick="showEdit(this);" onBlur="saveToDatabase(this,'address_1','<?php echo $id; ?>')">{{ $user->address_1 }}</td>
                                        <td contentEditable='true' onClick="showEdit(this);" onBlur="saveToDatabase(this,'address_2','<?php echo $id; ?>')">{{ $user->address_2 }}</td>
                                        <td contentEditable='true' onClick="showEdit(this);" onBlur="saveToDatabase(this,'pincode','<?php echo $id; ?>')">{{ $user->pincode }}</td>
                                       <!-- <td><button class="btn btn-default" id='modal' data-id="{{ $user->id }}" data-mandi="{{ $user->mandi_name }}" data-toggle="modal" data-target="#myModal">Edit</button></td>-->
                                    </tr>
                                    <?php /*if($count==1)
                                    {$count=2;}
                                    else
                                    {$count=1;}*/
                                    
                                    
                                    
                                    ?>
                                    
                                    @endforeach 
                                </tbody>
                            </table>
                            <!-- /.table-responsive -->
                            
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="../theme/vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../theme/vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../theme/vendor/metisMenu/metisMenu.min.js"></script>

    <!-- DataTables JavaScript -->
    <script src="../theme/vendor/datatables/js/jquery.dataTables.min.js"></script>
    <script src="../theme/vendor/datatables-plugins/dataTables.bootstrap.min.js"></script>
    <script src="../theme/vendor/datatables-responsive/dataTables.responsive.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../theme/dist/js/sb-admin-2.js"></script>

    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <script>

		function showEdit(editableObj) {
			$(editableObj).css("background","#FFF");
		} 
        $('#dataTables-example tbody').on( 'click', 'td', function () {
            //alert('Data:'+$(this).html().trim());
            //alert('Row:'+$(this).parent().find('td').html().trim());
            //alert('Column:'+$('#dataTables-example thead tr th').eq($(this).index()).html().trim());
            //if($('#dataTables-example thead tr th').eq($(this).index()).html().trim()=="Address 2")
            
            //alert("hi");
        });


		function saveToDatabase(editableObj,column,id) {
            var id = id;
            var column = column;
            var changed_val = editableObj.innerHTML;
           // alert(changed_val);
                $.ajax({
                type:'POST', 
                url:'/lsapp/public/getupd', 
                //data:'_token=<?php //echo csrf_token() ?>',
                data:{id:id,column:column,changed_val:changed_val,"_token": "{{ csrf_token() }}"},
                success:function(response){ 
                //$("#ajaxResponse").html(data.msg); 
               //if(response == "success")
                    //alert(response);
                    location.reload();
                } 
            
            }); 
		}
		</script>
    <script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
            responsive: true
        });
    });

    function showEdit(editableObj) {
        $(editableObj).css("background","#FFF");
    } 
   
    </script>


<!-- Modal -->
<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Insert Mandi Details</h4>
        </div>
        <div class="modal-body">
      <!--<form name="myform" id="myform" method="post">-->
        <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>"> 
        
        <div class="form-group">
            <label class="col-sm-3 control-label" for="textinput">Mandi Name</label>
            <div class="col-sm-9">
              <input type="text" name="mandi_name" id="mandi_name" placeholder="Mandi Name" class="form-control">
            </div><br><br>
            <div class="form-group">
            <label class="col-sm-3 control-label" for="textinput">Agmark ID</label>
            <div class="col-sm-9">
              <input type="text" name="agmark_market_id" id="agmark_market_id" placeholder="Agmark ID" class="form-control">
            </div><br><br>
            <div class="form-group">
            <label class="col-sm-3 control-label" for="textinput">Address</label>
            <div class="col-sm-9">
              <input type="text" name="address_1" id="address_1" placeholder="Address" class="form-control">
            </div><br><br>
            <div class="form-group">
            <label class="col-sm-3 control-label" for="textinput">Place</label>
            <div class="col-sm-9">
              <input type="text" name="address_2" id="address_2" placeholder="Place" class="form-control">
            </div><br><br>
            <div class="form-group">
            <label class="col-sm-3 control-label" for="textinput">Pincode</label>
            <div class="col-sm-9">
              <input type="text" name="pincode" id="pincode" placeholder="Pincode" class="form-control">
            </div>
          </div><br>
        <!--</form>-->
        </div>
        <div class="modal-footer">
        <input type='submit' class="btn btn-success" id="updmandi" value="Insert Details" />
          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
  <!-- Modal -->

</body>
<script>
    $(document).ready(function() {
        $("#updmandi").click(function(){
            var mandi_name = $("#mandi_name").val();
            var agmark_market_id = $("#agmark_market_id").val();
            var address_1 = $("#address_1").val();
            var address_2 = $("#address_2").val();
            var pincode = $("#pincode").val();

            if(mandi_name==""){alert("hi");return false;}
            else if(agmark_market_id==""){alert("hi");return false;}
            else if(address_1==""){alert("hi");return false;}
            else if(address_2==""){alert("hi");return false;}
            else if(pincode==""){alert("hi");return false;}
            else{
                $.ajax({
                type:'POST', 
                url:'/lsapp/public/getinsert', 
                //data:'_token=<?php //echo csrf_token() ?>',
                data:{mandi_name:mandi_name,agmark_market_id:agmark_market_id,address_1:address_1,address_1:address_1,address_2:address_2,pincode:pincode,"_token": "{{ csrf_token() }}"},
                success:function(response){ 
                
                    //alert(response);
                    location.reload();
                } 
            
            }); 
            }
        });
    }); 
</script>
</html>
